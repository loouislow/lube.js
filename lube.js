(function(_1,_2){
if(typeof define==="function"&&define.amd){
define(function(){
return _2(_1);
});
}else{
if(typeof exports==="object"){
module.exports=_2;
}else{
_1.lube=_2(_1);
}
}
})(this,function(_3){
"use strict";
var _4={};
var _5=function(){
};
var _6,_7,_8,_9,_a;
var _b=function(_c){
return (_c.offsetParent===null);
};
var _d=function(_e,_f){
if(_b(_e)){
return false;
}
var box=_e.getBoundingClientRect();
return (box.right>=_f.l&&box.bottom>=_f.t&&box.left<=_f.r&&box.top<=_f.b);
};
var _10=function(){
if(!_9&&!!_7){
return;
}
clearTimeout(_7);
_7=setTimeout(function(){
_4.render();
_7=null;
},_8);
};
_4.init=function(_11){
_11=_11||{};
var _12=_11.offset||0;
var _13=_11.offsetVertical||_12;
var _14=_11.offsetHorizontal||_12;
var _15=function(opt,_16){
return parseInt(opt||_16,10);
};
_6={t:_15(_11.offsetTop,_13),b:_15(_11.offsetBottom,_13),l:_15(_11.offsetLeft,_14),r:_15(_11.offsetRight,_14)};
_8=_15(_11.throttle,250);
_9=_11.debounce!==false;
_a=!!_11.unload;
_5=_11.callback||_5;
_4.render();
if(document.addEventListener){
_3.addEventListener("scroll",_10,false);
_3.addEventListener("load",_10,false);
}else{
_3.attachEvent("onscroll",_10);
_3.attachEvent("onload",_10);
}
};
_4.render=function(_17){
var _18=(_17||document).querySelectorAll("[data-lube], [data-lube-background]");
var _19=_18.length;
var src,_1a;
var _1b={l:0-_6.l,t:0-_6.t,b:(_3.innerHeight||document.documentElement.clientHeight)+_6.b,r:(_3.innerWidth||document.documentElement.clientWidth)+_6.r};
for(var i=0;i<_19;i++){
_1a=_18[i];
if(_d(_1a,_1b)){
if(_a){
_1a.setAttribute("data-lube-placeholder",_1a.src);
}
if(_1a.getAttribute("data-lube-background")!==null){
_1a.style.backgroundImage="url("+_1a.getAttribute("data-lube-background")+")";
}else{
if(_1a.src!==(src=_1a.getAttribute("data-lube"))){
_1a.src=src;
}
}
if(!_a){
_1a.removeAttribute("data-lube");
_1a.removeAttribute("data-lube-background");
}
_5(_1a,"load");
}else{
if(_a&&!!(src=_1a.getAttribute("data-lube-placeholder"))){
if(_1a.getAttribute("data-lube-background")!==null){
_1a.style.backgroundImage="url("+src+")";
}else{
_1a.src=src;
}
_1a.removeAttribute("data-lube-placeholder");
_5(_1a,"unload");
}
}
}
if(!_19){
_4.detach();
}
};
_4.detach=function(){
if(document.removeEventListener){
_3.removeEventListener("scroll",_10);
}else{
_3.detachEvent("onscroll",_10);
}
clearTimeout(_7);
};
return _4;
});
