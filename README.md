# lube.js

> Image lazy-load micro library with jQuery

To add an image directly into the page simply add a `data-lube` attribute to the img tag. To lazy load background images simply add a `data-lube-background' attribute to the element with the image URL.

# Example

```html
<body>

  <img src="img/blank.gif" alt="Photo" data-lube="img/photo.jpg">

  <script src="lube.js"></script>

  <script>
  lube.init({
    offset: 100,
    throttle: 250,
    unload: false,
    callback: function (element, op) {
      console.log(element, 'has been', op + 'ed')
    }
  });
  </script>
</body>
```

# Options
## .init()

The `init()` API takes a few options

#### offset
Type: `Number|String` Default: `0`

The `offset` option allows you to specify how far below, above, to the left, and to the right of the viewport you want Lube to _begin_ loading your images. If you specify `0`, Lube will load your image as soon as it is visible in the viewport, if you want to load _1000px_ below or above the viewport, use `1000`.

#### offsetVertical
Type: `Number|String` Default: `offset`'s value

The `offsetVertical` option allows you to specify how far above and below the viewport you want Lube to _begin_ loading your images.

#### offsetHorizontal
Type: `Number|String` Default: `offset`'s value

The `offsetHorizontal` option allows you to specify how far to the left and right of the viewport you want Lube to _begin_ loading your images.

#### offsetTop
Type: `Number|String` Default: `offsetVertical`'s value

The `offsetTop` option allows you to specify how far above the viewport you want Lube to _begin_ loading your images.

#### offsetBottom
Type: `Number|String` Default: `offsetVertical`'s value

The `offsetBottom` option allows you to specify how far below the viewport you want Lube to _begin_ loading your images.

#### offsetLeft
Type: `Number|String` Default: `offsetVertical`'s value

The `offsetLeft` option allows you to specify how far to left of the viewport you want Lube to _begin_ loading your images.

#### offsetRight
Type: `Number|String` Default: `offsetVertical`'s value

The `offsetRight` option allows you to specify how far to the right of the viewport you want Lube to _begin_ loading your images.

#### throttle
Type: `Number|String` Default: `250`

The throttle is managed by an internal function that prevents performance issues from continuous firing of `window.onscroll` events. Using a throttle will set a small timeout when the user scrolls and will keep throttling until the user stops. The default is `250` milliseconds.

#### debounce
Type: `Boolean` Default: `true`

By default the throttling function is actually a [debounce](http://underscorejs.org/#debounce) function so that the checking function is only triggered after a user stops scrolling. To use traditional throttling where it will only check the images every `throttle` milliseconds, set `debounce` to `false`.

#### unload
Type: `Boolean` Default: `false`

This option will tell lube to unload loaded images once they have scrolled beyond the viewport (including the offset area).

#### callback
Type: `Function`

The callback will be passed the element that has been updated and what the update operation was (ie `load` or `unload`). This can be useful if you want to add a class like `loaded` to the element. Or do some logging.

```js
lube.init({
  callback: function(element, op) {
    if(op === 'load') {
      element.classList.add('loaded');
    } else {
      element.classList.remove('loaded');
    }
  }
});
```
## .render()

Lube's callback `render()` can be used to make Lube poll your images when you're not scrolling, for instance if you've got a filter layout that swaps images but does not scroll, you need to call the internal functions without scrolling. Use `render()` for this:

```js
lube.render();
```

Using `render()` is also throttled, which means you can bind it to an `onresize` event and it will be optimised for performance in the same way `onscroll` is.
```
